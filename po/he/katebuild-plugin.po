# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Diego Iastrubni <elcuco@kde.org>, 2014.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
# SPDX-FileCopyrightText: 2024 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: katebuild-plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-14 00:40+0000\n"
"PO-Revision-Date: 2024-03-13 10:06+0200\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Lokalize 23.08.4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "צוות התרגום של KDE ישראל"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-l10n-he@kde.org"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "פלט"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "בנייה חוזרת"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "ביטול"

#: buildconfig.cpp:26
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr "להוסיף שגיאות ואזהרות לאבחון"

#: buildconfig.cpp:27
#, kde-format
msgid "Automatically switch to output pane on executing the selected target"
msgstr "להחליף ללוח הפלט בעת הרצה על היעד הנבחר"

#: buildconfig.cpp:44
#, kde-format
msgid "Build & Run"
msgstr "בנייה והרצה"

#: buildconfig.cpp:50
#, kde-format
msgid "Build & Run Settings"
msgstr "הגדרות בנייה והרצה"

#: plugin_katebuild.cpp:212 plugin_katebuild.cpp:219 plugin_katebuild.cpp:1242
#, kde-format
msgid "Build"
msgstr "בנייה"

#: plugin_katebuild.cpp:222
#, kde-format
msgid "Select Target..."
msgstr "בחירת יעד…"

#: plugin_katebuild.cpp:227
#, kde-format
msgid "Build Selected Target"
msgstr "בניית היעד הנבחר"

#: plugin_katebuild.cpp:232
#, kde-format
msgid "Build and Run Selected Target"
msgstr "בנייה והרצה של היעד הנבחר"

#: plugin_katebuild.cpp:237
#, kde-format
msgid "Stop"
msgstr "עצירה"

#: plugin_katebuild.cpp:242
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr "העברת המיקוד ללשונית הבאה משמאל"

#: plugin_katebuild.cpp:262
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr "העברת המיקוד ללשונית הבאה מימין"

#: plugin_katebuild.cpp:284
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "הגדרות של מטרות"

#: plugin_katebuild.cpp:323
#, kde-format
msgid ""
"<b>File not found:</b> %1<br><b>Search paths:</b><br>%2<br>Try adding a "
"search path to the \"Working Directory\""
msgstr ""

#: plugin_katebuild.cpp:410
#, kde-format
msgid "Build Information"
msgstr "פרטי בנייה"

#: plugin_katebuild.cpp:630
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "אין קובץ או ספרייה מוגדרת עבור בנייה"

#: plugin_katebuild.cpp:634
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr "הקובץ „%1” הוא לא קובץ מקומי. אי אפשר להדר קבצים שאינם מקומיים."

#: plugin_katebuild.cpp:696
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""
"לא ניתן להריץ את הפקודה: %1\n"
"נתיב העבודה לא קיים: %2"

#: plugin_katebuild.cpp:710
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "אי אפשר להפעיל את „%1”. מצב סיום = %2"

#: plugin_katebuild.cpp:725
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "הבנייה של <b>%1</b> בוטלה"

#: plugin_katebuild.cpp:832
#, kde-format
msgid "No target available for building."
msgstr "אין יעד זמין לבנייה."

#: plugin_katebuild.cpp:846
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "לא הוגדרו קובץ או תיקייה מקומיים לבנייה."

#: plugin_katebuild.cpp:852
#, kde-format
msgid "Already building..."
msgstr "כבר מתבצעת בנייה…"

#: plugin_katebuild.cpp:874
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "המטרה <b>%1</b> נבנית…"

#: plugin_katebuild.cpp:888
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr "<title>תוצאות ה־Make (הכנה):</title><nl/>%1"

#: plugin_katebuild.cpp:924
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr "בניית <b>%1</b> הושלמה. %2 שגיאות, %3 אזהרות, %4 הערות"

#: plugin_katebuild.cpp:930
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "נמצאה שגיאה אחת."
msgstr[1] "נמצאו שתי שגיאות."
msgstr[2] "נמצאו %1 שגיאות."
msgstr[3] "נמצאו %1 שגיאות."

#: plugin_katebuild.cpp:934
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "נמצאה אזהרה אחת."
msgstr[1] "נמצאו שתי אזהרות."
msgstr[2] "נמצאו %1 אזהרות."
msgstr[3] "נמצאו %1 אזהרות."

#: plugin_katebuild.cpp:937
#, kde-format
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "נמצאה הערה אחת."
msgstr[1] "נמצאו שתי הערות."
msgstr[2] "נמצאו %1 הערות."
msgstr[3] "נמצאו %1 הערות."

#: plugin_katebuild.cpp:942
#, kde-format
msgid "Build failed."
msgstr "הבנייה נכשלה"

#: plugin_katebuild.cpp:944
#, kde-format
msgid "Build completed without problems."
msgstr "הבנייה הסתיימה ללא בעיות"

#: plugin_katebuild.cpp:949
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr "בניית <b>%1 נכשלה</b>. %2 שגיאות, %3 אזהרות %4 הערות"

#: plugin_katebuild.cpp:973
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr "לא ניתן להריץ: %1 לא הוגדרה תיקיית עבודה."

#: plugin_katebuild.cpp:1199
#, kde-format
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "error"

#: plugin_katebuild.cpp:1202
#, kde-format
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "warning"

#: plugin_katebuild.cpp:1205
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr "note|info"

#: plugin_katebuild.cpp:1208
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "undefined reference"

#: plugin_katebuild.cpp:1241 TargetModel.cpp:285 TargetModel.cpp:297
#, kde-format
msgid "Target Set"
msgstr "קבוצת יעדים"

#: plugin_katebuild.cpp:1243
#, kde-format
msgid "Clean"
msgstr "ניקוי"

#: plugin_katebuild.cpp:1244
#, kde-format
msgid "Config"
msgstr "הגדרה"

#: plugin_katebuild.cpp:1245
#, kde-format
msgid "ConfigClean"
msgstr "ניקוי הגדרות (ConfigClean)"

#: plugin_katebuild.cpp:1436
#, kde-format
msgid "Cannot save build targets in: %1"
msgstr "לא ניתן לשמור את יעדי הבנייה תחת: %1"

#: TargetHtmlDelegate.cpp:51
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr "<B>י:</B> %1"

#: TargetHtmlDelegate.cpp:53
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>תיקייה:</B> %1"

#: TargetHtmlDelegate.cpp:104
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""
"להשאיר ריק כדי שייעשה שימוש בתיקיית המסמך הנוכחי.\n"
"אפשר להוסיף תיקיות חיפוש על ידי הוספת נתיבים עם ‚;’ ביניהם."

#: TargetHtmlDelegate.cpp:111
#, kde-format
msgid ""
"Use:\n"
"\"%B\" for project base directory\n"
"\"%b\" for name of project base directory"
msgstr ""
"שימוש:\n"
"„%B” לייצוג תיקיית הבסיס של המיזם\n"
"„%b” לייצוג שם תיקיית הבסיס של המיזם"

#: TargetHtmlDelegate.cpp:114
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"שימוש:\n"
"„%f” לייצוג הקובץ הנוכחי\n"
"„%d” לתיקיית הקובץ הנוכחי\n"
"„%n” לשם הקובץ הנוכחי בלי סיומת"

#: TargetModel.cpp:530
#, kde-format
msgid "Project"
msgstr "מיזם"

#: TargetModel.cpp:530
#, kde-format
msgid "Session"
msgstr "הפעלה"

#: TargetModel.cpp:624
#, kde-format
msgid "Command/Target-set Name"
msgstr "שם פקודה/קבוצת יעדים"

#: TargetModel.cpp:627
#, kde-format
msgid "Working Directory / Command"
msgstr "תיקיית עבודה / פקודה"

#: TargetModel.cpp:630
#, kde-format
msgid "Run Command"
msgstr "הרצת פקודות"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr "סינון יעדים, יש להשתמש במקשי החצים כדי לבחור, להיכנס או להריץ"

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "יצירת קבוצת מטרות חדשה"

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr "העתקת פקודת או קבוצת יעדים"

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr "מחיקת היעד או את קבוצת היעדים"

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "הוספת מטרה חדשה"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "בניית המטרה הנבחרת"

#: targets.cpp:48
#, kde-format
msgid "Build and run selected target"
msgstr "בנייה והרצה של היעד הנבחר"

#: targets.cpp:52
#, kde-format
msgid "Move selected target up"
msgstr "העלאת היעד הנבחר למעלה"

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr "הורדת היעד הנבחר למטה"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "&בנייה"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "הוספת נתיב"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "בחירת תיקייה להוספה"

#~ msgid "Project Plugin Targets"
#~ msgstr "מטרות תוסף פרוייקטים"

#, fuzzy
#~ msgid "build"
#~ msgstr "&בנה"

#, fuzzy
#~ msgid "clean"
#~ msgstr "נקה"

#~ msgid "Building <b>%1</b> completed."
#~ msgstr "בניית המטרה <b>%1</b> הושלמה."

#~ msgid "Building <b>%1</b> had errors."
#~ msgstr "לבנייה של <b>%1</b> היו שגיאות."

#, fuzzy
#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "לבנייה של <b>%1</b> היו שגיאות."

#~ msgid "Show:"
#~ msgstr "ה&צג:"

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "קובץ"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "קו"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "הודעה"

#~ msgid "Next Error"
#~ msgstr "שגיאה הבאה"

#~ msgid "Previous Error"
#~ msgstr "שגיאה קודמת"

#, fuzzy
#~| msgctxt "The same word as 'make' uses to mark an error."
#~| msgid "error"
#~ msgid "Error"
#~ msgstr "שגיאה"

#, fuzzy
#~| msgctxt "The same word as 'make' uses to mark a warning."
#~| msgid "warning"
#~ msgid "Warning"
#~ msgstr "הזהרה"

#~ msgid "Only Errors"
#~ msgstr "שגיאות בלבד"

#~ msgid "Errors and Warnings"
#~ msgstr "שגיאות והזהרות"

#~ msgid "Parsed Output"
#~ msgstr "הפלט המנותח"

#~ msgid "Full Output"
#~ msgstr "הפלט המלא"

#, fuzzy
#~ msgid "Select active target set"
#~ msgstr "מחק את המטרה הנבחרת"

#, fuzzy
#~| msgid "Build selected target"
#~ msgid "Filter targets"
#~ msgstr "בנה את המטרה הנבחרת"

#~ msgid "Build Default Target"
#~ msgstr "בנה את מטרת ברירת מחדל"

#, fuzzy
#~| msgid "Build Default Target"
#~ msgid "Build and Run Default Target"
#~ msgstr "בנה את מטרת ברירת מחדל"

#, fuzzy
#~ msgid "Build Previous Target"
#~ msgstr "בנה את המטרה הקודמת שוב"

#, fuzzy
#~ msgid "Kate Build Plugin"
#~ msgstr "תוסף בנייה"

#, fuzzy
#~ msgid "Select build target"
#~ msgstr "מחק את המטרה הנבחרת"

#~ msgid "Build Output"
#~ msgstr "פלט של בנייה"
